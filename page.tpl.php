<?php // $Id$   ?>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
	<head>
		<?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body class="<?php print $body_classes ?>">
    <p><a id="top"></a></p>
    <div id="wrapper-header">
      <div id="header"> 
        <?php if($is_front): ?>
          <h1 id="site-name">
            <a href="<?php print $base_path; ?>" title="<?php print $site_name; ?>">
            <?php if ($logo): ?>
            	<img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" id="logo" />
            <?php endif; ?>
            <?php print $site_name; ?></a>
          </h1>
        <?php else: ?>
          <div id="site-name">
            <a href="<?php print $base_path; ?>" title="<?php print $site_name; ?>">
            <?php if ($logo): ?>
            	<img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" id="logo" />
            <?php endif; ?>
            <?php print $site_name; ?></a>
          </div>
        <?php endif; ?>
				<?php if ($site_slogan): ?>
					<h2 id='site-slogan'>
						<?php print $site_slogan; ?>
					</h2>
				<?php endif; ?> <!-- /logo-name-and-slogan -->
          <?php if($search_box): ?>
            <?php print $search_box; ?>
          <?php endif; ?>
          
				<?php if ($primary_links) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        <?php if ($secondary_links) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?> <!-- /primary-and-secondary-links -->
      </div><!-- /header -->
    </div><!-- /wrapper-header -->

    <div id="wrapper-main"> 
      <div id="main"> 
	    <?php if ($header): ?>
				<div id="topbar">
					<?php print $header; ?>
				</div>
	    <?php endif; ?> 
		
        <div id="content">
          <div id="center">
						<?php if ($breadcrumb): print $breadcrumb; endif; ?>
						<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
						<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
            <?php if ($title && !$is_front): ?>
              <?php print '<h1'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h1>'; ?>
						<?php endif; ?>
            <?php if ($tabs): print $tabs .'</div>'; endif; ?>
						<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
						<?php if ($help): print $help; endif; ?>
						<?php if ($messages): print $messages; endif; ?>
						<?php print $content ?>
						<?php print $feed_icons ?>
						<p class="clear-block"><a href="#top" class="to-top"><?php print t('Back to top'); ?></a></p>
          </div><!-- /center -->
					
          <?php if ($right): ?>
						<div id="sidebar-right" class="sidebar">
							<?php print $right ?>
						</div><!-- /sidebar-right -->
          <?php endif; ?>
        </div><!-- /content -->
		
		<?php if ($left): ?>
        <div id="sidebar-left" class="sidebar">
		  <?php print $left ?>
        </div><!-- /sidebar-left -->
        <?php endif; ?>
      </div><!-- /main -->

	  <div id="footer">
      <?php if ($footer):?>
        <?php print $footer; ?>
      <?php endif; ?>
      
      <?php print $footer_message ?>
				
	  </div>
    </div><!-- /wrapper-main -->
	
  <?php print $closure ?>	
  </body>
</html>