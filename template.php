<?php

/**
 * Custom Breadcrumb
 *
 */
function aberdeen_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('aberdeen_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('aberdeen_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('aberdeen_breadcrumb_title')) {
        $trailing_separator = $breadcrumb_separator;
        $title = menu_get_active_title();
      }
      elseif (theme_get_setting('aberdeen_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }
      return '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . "$trailing_separator$title</div>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Allow themable wrapping of all comments.
 */
function aberdeen_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">". $primary ."</ul>";
  }
  $output .= "";
  return $output;
}

/**
 * Override theme_links to include <span> in list.
 */
if (!module_exists('menutrails')) {
  function phptemplate_links($links, $attributes = array('class' => 'links')) {
    $output = '';
  
    if (count($links) > 0) {
      $output = '<ul'. drupal_attributes($attributes) .'>';
  
      $num_links = count($links);
      $i = 1;
  
      foreach ($links as $key => $link) {
        $class = '';
  
        // Automatically add a class to each link and also to each LI
        if (isset($link['attributes']) && isset($link['attributes']['class'])) {
          $link['attributes']['class'] .= ' ' . $key;
          $class = $key;
        }
        else {
          $link['attributes']['class'] = $key;
          $class = $key;
        }
  
        // Add first and last classes to the list of links to help out themers.
        $extra_class = '';
        if ($i == 1) {
          $extra_class .= 'first ';
        }
        if ($i == $num_links) {
          $extra_class .= 'last ';
        }
      
     
      // Add class active to active li 
      $current = '';
      if (strstr($class, 'active')) {
        $current = ' active';
      }	
  
      $output .= '<li class="'. $extra_class . $class . $current .'"><span>';
      
      // Is the title HTML?
        $html = isset($link['html']) && $link['html'];
  
        // Initialize fragment and query variables.
        $link['query'] = isset($link['query']) ? $link['query'] : NULL;
        $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;
  
        if (isset($link['href'])) {
           $output .= l($link['title'], $link['href'], array('attributes' => $link['attributes'],
           'query' => $link['query'], 'fragment' => $link['fragment'], 'html' => $html));
        }
        else if ($link['title']) {
          //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
          if (!$html) {
            $link['title'] = check_plain($link['title']);
          }
          $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
        }
  
        $i++;
        $output .= "</span></li>";
      }
      $output .= "";
      $output .= '</ul>';
    }
  
    return $output;
  }
}
